ADDITIONAL_BUILD_PROPERTIES += \
  ro.ag.version=$(AG_DISPLAY_VERSION) \
  ro.ag.build.status=$(AG_BUILDTYPE) \
  ro.ag.changelog.version=Changelog-$(AG_VERSION) \
  ro.ag.fingerprint=$(AG_FINGERPRINT) \
  ro.ag.static.version=$(AG_VERSION_STATIC)
