
# autopatch.sh: script to manage patches on top of repo
# Copyright (C) 2019 Intel Corporation. All rights reserved.
# Author: sgnanase <sundar.gnanasekaran@intel.com>
# Author: Sun, Yi J <yi.j.sun@intel.com>
#
# SPDX-License-Identifier: BSD-3-Clause

# save the official lunch command to aosp_lunch() and source it
tmp_lunch=`mktemp`
rompath=$(pwd)
sed '/ lunch()/,/^}/!d'  build/envsetup.sh | sed 's/function lunch/function aosp_lunch/' > ${tmp_lunch}
source ${tmp_lunch}
rm -f ${tmp_lunch}
vendor_path="android-generic"

function apply-x86-patches
{

	vendor/${vendor_path}/autopatch_x86.sh

}

function apply-treble-patches
{

	vendor/${vendor_path}/autopatch_treble.sh

}

function apply-manifest-patch-pc
{

	vendor/${vendor_path}/manifest_prepatch_pc.sh

}

function get-cros-files-x86
{
	echo "Setting up Proprietary environment for: $1"
	lunch android_x86-userdebug
	echo "Building proprietary tools... This might take a little..."
	echo "Be prepared to enter root password in order to mount the cros images and unpack things"
	cd vendor/google/chromeos-x86
	./extract-files.sh
	cd ..
	cd ..
	cd ..
}

function get-cros-files-x86_64
{
	echo "Setting up Proprietary environment for: $1"
	lunch android_x86_64-userdebug
	echo "Building proprietary tools... This might take a little..."
	echo "Be prepared to enter root password in order to mount the cros images and unpack things"
	cd vendor/google/chromeos-x86
	./extract-files.sh
	cd ..
	cd ..
	cd ..
}

function build-x86()
{
	bash vendor/${vendor_path}/build-x86.sh $1 $2 $3 $4
}

function build-treble()
{
	bash vendor/${vendor_path}/build-treble.sh $1 $2 $3 $4
}

function build-all-treble()
{
	# Check for folder
	if [ -d $rompath/treble-releases/$1 ]; then
		echo "There looks to already be a folder setup for this. moving on"
	else
		echo "making $rompath/treble-releases/$1"
		mkdir -p $rompath/treble-releases/$1
	fi
	
	# Run the builds for:
	
	# Gapps
	if [ "$1" = "gapps" ]; then
		# arm64_a_gapps
		echo "Cleaning up first"
		make clean 
		build-treble arm64_a_gapps
		if [ -d $rompath/treble-releases/$1/phhgsi_arm64_a ]; then
			echo "found $rompath/treble-releases/$1/phhgsi_arm64_a"
		else
			mkdir -p $rompath/treble-releases/$1/phhgsi_arm64_a
		fi
		echo "Copying files to treble-releases"
		cp $rompath/out/target/product/phhgsi_arm64_a/*.img $rompath/treble-releases/$1/phhgsi_arm64_a
		cp $rompath/out/target/product/phhgsi_arm64_a/*.md5 $rompath/treble-releases/$1/phhgsi_arm64_a
		cp $rompath/out/target/product/phhgsi_arm64_a/*hangelog* $rompath/treble-releases/$1/phhgsi_arm64_a
		
		# arm64_ab_gapps
		echo "Cleaning up first"
		make clean 
		build-treble arm64_ab_gapps 
		if [ -d $rompath/treble-releases/$1/phhgsi_arm64_ab ]; then
			echo "found $rompath/treble-releases/$1/phhgsi_arm64_ab"
		else
			mkdir -p $rompath/treble-releases/$1/phhgsi_arm64_ab
		fi
		echo "Copying files to treble-releases"
		cp $rompath/out/target/product/phhgsi_arm64_ab/*.img $rompath/treble-releases/$1/phhgsi_arm64_ab
		cp $rompath/out/target/product/phhgsi_arm64_ab/*.md5 $rompath/treble-releases/$1/phhgsi_arm64_ab
		cp $rompath/out/target/product/phhgsi_arm64_ab/*hangelog* $rompath/treble-releases/$1/phhgsi_arm64_ab
		
		# arm_a_gapps
		echo "Cleaning up first"
		make clean 
		build-treble arm_a_gapps
		if [ -d $rompath/treble-releases/$1/phhgsi_arm_a ]; then
			echo "found $rompath/treble-releases/$1/phhgsi_arm_a"
		else
			mkdir -p $rompath/treble-releases/$1/phhgsi_arm_a
		fi
		echo "Copying files to treble-releases"
		cp $rompath/out/target/product/phhgsi_arm_a/*.img $rompath/treble-releases/$1/phhgsi_arm_a
		cp $rompath/out/target/product/phhgsi_arm_a/*.md5 $rompath/treble-releases/$1/phhgsi_arm_a
		cp $rompath/out/target/product/phhgsi_arm_a/*hangelog* $rompath/treble-releases/$1/phhgsi_arm_a
		
		# arm_ab_gapps
		echo "Cleaning up first"
		make clean 
		build-treble arm_ab_gapps 
		if [ -d $rompath/treble-releases/$1/phhgsi_arm_ab ]; then
			echo "found $rompath/treble-releases/$1/phhgsi_arm_ab"
		else
			mkdir -p $rompath/treble-releases/$1/phhgsi_arm_ab
		fi
		echo "Copying files to treble-releases"
		cp $rompath/out/target/product/phhgsi_arm_ab/*.img $rompath/treble-releases/$1/phhgsi_arm_ab
		cp $rompath/out/target/product/phhgsi_arm_ab/*.md5 $rompath/treble-releases/$1/phhgsi_arm_ab
		cp $rompath/out/target/product/phhgsi_arm_ab/*hangelog* $rompath/treble-releases/$1/phhgsi_arm_ab
		
		# a64_a_gapps
		echo "Cleaning up first"
		make clean 
		build-treble a64_a_gapps
		if [ -d $rompath/treble-releases/$1/phhgsi_a64_a ]; then
			echo "found $rompath/treble-releases/$1/phhgsi_a64_a"
		else
			mkdir -p $rompath/treble-releases/$1/phhgsi_a64_a
		fi
		echo "Copying files to treble-releases"
		cp $rompath/out/target/product/phhgsi_a64_a/*.img $rompath/treble-releases/$1/phhgsi_a64_a
		cp $rompath/out/target/product/phhgsi_a64_a/*.md5 $rompath/treble-releases/$1/phhgsi_a64_a
		cp $rompath/out/target/product/phhgsi_a64_a/*hangelog* $rompath/treble-releases/$1/phhgsi_a64_a
		# a64_ab_gapps
		echo "Cleaning up first"
		make clean 
		build-treble a64_ab_gapps 
		if [ -d $rompath/treble-releases/$1/phhgsi_a64_ab ]; then
			echo "found $rompath/treble-releases/$1/phhgsi_a64_ab"
		else
			mkdir -p $rompath/treble-releases/$1/phhgsi_a64_ab
		fi
		echo "Copying files to treble-releases"
		cp $rompath/out/target/product/phhgsi_a64_ab/*.img $rompath/treble-releases/$1/phhgsi_a64_ab
		cp $rompath/out/target/product/phhgsi_a64_ab/*.md5 $rompath/treble-releases/$1/phhgsi_a64_ab
		cp $rompath/out/target/product/phhgsi_a64_ab/*hangelog* $rompath/treble-releases/$1/phhgsi_a64_ab
	fi
	
	# foss
	if [ "$1" = "foss" ]; then
		# arm64_a_foss
		echo "Cleaning up first"
		make clean 
		build-treble arm64_a_foss
		if [ -d $rompath/treble-releases/$1/phhgsi_arm64_a ]; then
			echo "found $rompath/treble-releases/$1/phhgsi_arm64_a"
		else
			mkdir -p $rompath/treble-releases/$1/phhgsi_arm64_a
		fi
		echo "Copying files to treble-releases"
		cp $rompath/out/target/product/phhgsi_arm64_a/*.img $rompath/treble-releases/$1/phhgsi_arm64_a
		cp $rompath/out/target/product/phhgsi_arm64_a/*.md5 $rompath/treble-releases/$1/phhgsi_arm64_a
		cp $rompath/out/target/product/phhgsi_arm64_a/*hangelog* $rompath/treble-releases/$1/phhgsi_arm64_a
		# arm64_ab_foss
		echo "Cleaning up first"
		make clean 
		build-treble arm64_ab_foss 
		if [ -d $rompath/treble-releases/$1/phhgsi_arm64_ab ]; then
			echo "found $rompath/treble-releases/$1/phhgsi_arm64_ab"
		else
			mkdir -p $rompath/treble-releases/$1/phhgsi_arm64_ab
		fi
		echo "Copying files to treble-releases"
		cp $rompath/out/target/product/phhgsi_arm64_ab/*.img $rompath/treble-releases/$1/phhgsi_arm64_ab
		cp $rompath/out/target/product/phhgsi_arm64_ab/*.md5 $rompath/treble-releases/$1/phhgsi_arm64_ab
		cp $rompath/out/target/product/phhgsi_arm64_ab/*hangelog* $rompath/treble-releases/$1/phhgsi_arm64_ab
		
		# arm_a_foss
		echo "Cleaning up first"
		make clean 
		build-treble arm_a_foss
		if [ -d $rompath/treble-releases/$1/phhgsi_arm_a ]; then
			echo "found $rompath/treble-releases/$1/phhgsi_arm_a"
		else
			mkdir -p $rompath/treble-releases/$1/phhgsi_arm_a
		fi
		echo "Copying files to treble-releases"
		cp $rompath/out/target/product/phhgsi_arm_a/*.img $rompath/treble-releases/$1/phhgsi_arm_a
		cp $rompath/out/target/product/phhgsi_arm_a/*.md5 $rompath/treble-releases/$1/phhgsi_arm_a
		cp $rompath/out/target/product/phhgsi_arm_a/*hangelog* $rompath/treble-releases/$1/phhgsi_arm_a
		# arm_ab_foss
		echo "Cleaning up first"
		make clean 
		build-treble arm_ab_foss 
		if [ -d $rompath/treble-releases/$1/phhgsi_arm_ab ]; then
			echo "found $rompath/treble-releases/$1/phhgsi_arm_ab"
		else
			mkdir -p $rompath/treble-releases/$1/phhgsi_arm_ab
		fi
		echo "Copying files to treble-releases"
		cp $rompath/out/target/product/phhgsi_arm_ab/*.img $rompath/treble-releases/$1/phhgsi_arm_ab
		cp $rompath/out/target/product/phhgsi_arm_ab/*.md5 $rompath/treble-releases/$1/phhgsi_arm_ab
		cp $rompath/out/target/product/phhgsi_arm_ab/*hangelog* $rompath/treble-releases/$1/phhgsi_arm_ab
		
		# a64_a_foss
		echo "Cleaning up first"
		make clean 
		build-treble a64_a_foss
		if [ -d $rompath/treble-releases/$1/phhgsi_a64_a ]; then
			echo "found $rompath/treble-releases/$1/phhgsi_a64_a"
		else
			mkdir -p $rompath/treble-releases/$1/phhgsi_a64_a
		fi
		echo "Copying files to treble-releases"
		cp $rompath/out/target/product/phhgsi_a64_a/*.img $rompath/treble-releases/$1/phhgsi_a64_a
		cp $rompath/out/target/product/phhgsi_a64_a/*.md5 $rompath/treble-releases/$1/phhgsi_a64_a
		cp $rompath/out/target/product/phhgsi_a64_a/*hangelog* $rompath/treble-releases/$1/phhgsi_a64_a
		# a64_ab_foss
		echo "Cleaning up first"
		make clean 
		build-treble a64_ab_foss 
		if [ -d $rompath/treble-releases/$1/phhgsi_a64_ab ]; then
			echo "found $rompath/treble-releases/$1/phhgsi_a64_ab"
		else
			mkdir -p $rompath/treble-releases/$1/phhgsi_a64_ab
		fi
		echo "Copying files to treble-releases"
		cp $rompath/out/target/product/phhgsi_a64_ab/*.img $rompath/treble-releases/$1/phhgsi_a64_ab
		cp $rompath/out/target/product/phhgsi_a64_ab/*.md5 $rompath/treble-releases/$1/phhgsi_a64_ab
		cp $rompath/out/target/product/phhgsi_a64_ab/*hangelog* $rompath/treble-releases/$1/phhgsi_a64_ab
	fi
	
	# stock
	if [ "$1" = "stock" ]; then
		# arm64_a_stock
		echo "Cleaning up first"
		make clean 
		build-treble arm64_a_stock
		if [ -d $rompath/treble-releases/$1/phhgsi_arm64_a ]; then
			echo "found $rompath/treble-releases/$1/phhgsi_arm64_a"
		else
			mkdir -p $rompath/treble-releases/$1/phhgsi_arm64_a
		fi
		echo "Copying files to treble-releases"
		cp $rompath/out/target/product/phhgsi_arm64_a/*.img $rompath/treble-releases/$1/phhgsi_arm64_a
		cp $rompath/out/target/product/phhgsi_arm64_a/*.md5 $rompath/treble-releases/$1/phhgsi_arm64_a
		cp $rompath/out/target/product/phhgsi_arm64_a/*hangelog* $rompath/treble-releases/$1/phhgsi_arm64_a
		# arm64_ab_stock
		echo "Cleaning up first"
		make clean 
		build-treble arm64_ab_stock 
		if [ -d $rompath/treble-releases/$1/phhgsi_arm64_ab ]; then
			echo "found $rompath/treble-releases/$1/phhgsi_arm64_ab"
		else
			mkdir -p $rompath/treble-releases/$1/phhgsi_arm64_ab
		fi
		echo "Copying files to treble-releases"
		cp $rompath/out/target/product/phhgsi_arm64_ab/*.img $rompath/treble-releases/$1/phhgsi_arm64_ab
		cp $rompath/out/target/product/phhgsi_arm64_ab/*.md5 $rompath/treble-releases/$1/phhgsi_arm64_ab
		cp $rompath/out/target/product/phhgsi_arm64_ab/*hangelog* $rompath/treble-releases/$1/phhgsi_arm64_ab
		
		# arm_a_stock
		echo "Cleaning up first"
		make clean 
		build-treble arm_a_stock
		if [ -d $rompath/treble-releases/$1/phhgsi_arm_a ]; then
			echo "found $rompath/treble-releases/$1/phhgsi_arm_a"
		else
			mkdir -p $rompath/treble-releases/$1/phhgsi_arm_a
		fi
		echo "Copying files to treble-releases"
		cp $rompath/out/target/product/phhgsi_arm_a/*.img $rompath/treble-releases/$1/phhgsi_arm_a
		cp $rompath/out/target/product/phhgsi_arm_a/*.md5 $rompath/treble-releases/$1/phhgsi_arm_a
		cp $rompath/out/target/product/phhgsi_arm_a/*hangelog* $rompath/treble-releases/$1/phhgsi_arm_a
		# arm_ab_stock
		echo "Cleaning up first"
		make clean 
		build-treble arm_ab_stock 
		if [ -d $rompath/treble-releases/$1/phhgsi_arm_ab ]; then
			echo "found $rompath/treble-releases/$1/phhgsi_arm_ab"
		else
			mkdir -p $rompath/treble-releases/$1/phhgsi_arm_ab
		fi
		echo "Copying files to treble-releases"
		cp $rompath/out/target/product/phhgsi_arm_ab/*.img $rompath/treble-releases/$1/phhgsi_arm_ab
		cp $rompath/out/target/product/phhgsi_arm_ab/*.md5 $rompath/treble-releases/$1/phhgsi_arm_ab
		cp $rompath/out/target/product/phhgsi_arm_ab/*hangelog* $rompath/treble-releases/$1/phhgsi_arm_ab
		
		# a64_a_stock
		echo "Cleaning up first"
		make clean 
		build-treble a64_a_stock
		if [ -d $rompath/treble-releases/$1/phhgsi_a64_a ]; then
			echo "found $rompath/treble-releases/$1/phhgsi_a64_a"
		else
			mkdir -p $rompath/treble-releases/$1/phhgsi_a64_a
		fi
		echo "Copying files to treble-releases"
		cp $rompath/out/target/product/phhgsi_a64_a/*.img $rompath/treble-releases/$1/phhgsi_a64_a
		cp $rompath/out/target/product/phhgsi_a64_a/*.md5 $rompath/treble-releases/$1/phhgsi_a64_a
		cp $rompath/out/target/product/phhgsi_a64_a/*hangelog* $rompath/treble-releases/$1/phhgsi_a64_a
		# a64_ab_stock
		echo "Cleaning up first"
		make clean 
		build-treble a64_ab_stock 
		if [ -d $rompath/treble-releases/$1/phhgsi_a64_ab ]; then
			echo "found $rompath/treble-releases/$1/phhgsi_a64_ab"
		else
			mkdir -p $rompath/treble-releases/$1/phhgsi_a64_ab
		fi
		echo "Copying files to treble-releases"
		cp $rompath/out/target/product/phhgsi_a64_ab/*.img $rompath/treble-releases/$1/phhgsi_a64_ab
		cp $rompath/out/target/product/phhgsi_a64_ab/*.md5 $rompath/treble-releases/$1/phhgsi_a64_ab
		cp $rompath/out/target/product/phhgsi_a64_ab/*hangelog* $rompath/treble-releases/$1/phhgsi_a64_ab
	fi
	
	# go
	if [ "$1" = "go" ]; then
		# arm64_a_go
		echo "Cleaning up first"
		make clean 
		build-treble arm64_a_go
		if [ -d $rompath/treble-releases/$1/phhgsi_arm64_a ]; then
			echo "found $rompath/treble-releases/$1/phhgsi_arm64_a"
		else
			mkdir -p $rompath/treble-releases/$1/phhgsi_arm64_a
		fi
		echo "Copying files to treble-releases"
		cp $rompath/out/target/product/phhgsi_arm64_a/*.img $rompath/treble-releases/$1/phhgsi_arm64_a
		cp $rompath/out/target/product/phhgsi_arm64_a/*.md5 $rompath/treble-releases/$1/phhgsi_arm64_a
		cp $rompath/out/target/product/phhgsi_arm64_a/*hangelog* $rompath/treble-releases/$1/phhgsi_arm64_a
		# arm64_ab_go
		echo "Cleaning up first"
		make clean 
		build-treble arm64_ab_go 
		if [ -d $rompath/treble-releases/$1/phhgsi_arm64_ab ]; then
			echo "found $rompath/treble-releases/$1/phhgsi_arm64_ab"
		else
			mkdir -p $rompath/treble-releases/$1/phhgsi_arm64_ab
		fi
		echo "Copying files to treble-releases"
		cp $rompath/out/target/product/phhgsi_arm64_ab/*.img $rompath/treble-releases/$1/phhgsi_arm64_ab
		cp $rompath/out/target/product/phhgsi_arm64_ab/*.md5 $rompath/treble-releases/$1/phhgsi_arm64_ab
		cp $rompath/out/target/product/phhgsi_arm64_ab/*hangelog* $rompath/treble-releases/$1/phhgsi_arm64_ab
		
		# arm_a_go
		echo "Cleaning up first"
		make clean 
		build-treble arm_a_go
		if [ -d $rompath/treble-releases/$1/phhgsi_arm_a ]; then
			echo "found $rompath/treble-releases/$1/phhgsi_arm_a"
		else
			mkdir -p $rompath/treble-releases/$1/phhgsi_arm_a
		fi
		echo "Copying files to treble-releases"
		cp $rompath/out/target/product/phhgsi_arm_a/*.img $rompath/treble-releases/$1/phhgsi_arm_a
		cp $rompath/out/target/product/phhgsi_arm_a/*.md5 $rompath/treble-releases/$1/phhgsi_arm_a
		cp $rompath/out/target/product/phhgsi_arm_a/*hangelog* $rompath/treble-releases/$1/phhgsi_arm_a
		# arm_ab_go
		echo "Cleaning up first"
		make clean 
		build-treble arm_ab_go 
		if [ -d $rompath/treble-releases/$1/phhgsi_arm_ab ]; then
			echo "found $rompath/treble-releases/$1/phhgsi_arm_ab"
		else
			mkdir -p $rompath/treble-releases/$1/phhgsi_arm_ab
		fi
		echo "Copying files to treble-releases"
		cp $rompath/out/target/product/phhgsi_arm_ab/*.img $rompath/treble-releases/$1/phhgsi_arm_ab
		cp $rompath/out/target/product/phhgsi_arm_ab/*.md5 $rompath/treble-releases/$1/phhgsi_arm_ab
		cp $rompath/out/target/product/phhgsi_arm_ab/*hangelog* $rompath/treble-releases/$1/phhgsi_arm_ab
		
		# a64_a_go
		echo "Cleaning up first"
		make clean 
		build-treble a64_a_go
		if [ -d $rompath/treble-releases/$1/phhgsi_a64_a ]; then
			echo "found $rompath/treble-releases/$1/phhgsi_a64_a"
		else
			mkdir -p $rompath/treble-releases/$1/phhgsi_a64_a
		fi
		echo "Copying files to treble-releases"
		cp $rompath/out/target/product/phhgsi_a64_a/*.img $rompath/treble-releases/$1/phhgsi_a64_a
		cp $rompath/out/target/product/phhgsi_a64_a/*.md5 $rompath/treble-releases/$1/phhgsi_a64_a
		cp $rompath/out/target/product/phhgsi_a64_a/*hangelog* $rompath/treble-releases/$1/phhgsi_a64_a
		# a64_ab_go
		echo "Cleaning up first"
		make clean 
		build-treble a64_ab_go 
		if [ -d $rompath/treble-releases/$1/phhgsi_a64_ab ]; then
			echo "found $rompath/treble-releases/$1/phhgsi_a64_ab"
		else
			mkdir -p $rompath/treble-releases/$1/phhgsi_a64_ab
		fi
		echo "Copying files to treble-releases"
		cp $rompath/out/target/product/phhgsi_a64_ab/*.img $rompath/treble-releases/$1/phhgsi_a64_ab
		cp $rompath/out/target/product/phhgsi_a64_ab/*.md5 $rompath/treble-releases/$1/phhgsi_a64_ab
		cp $rompath/out/target/product/phhgsi_a64_ab/*hangelog* $rompath/treble-releases/$1/phhgsi_a64_ab
	fi

}
